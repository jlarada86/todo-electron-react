import {
  SET_TODOS_LIST,
  ADD_TODO,
  UPDATE_TODO,
  DELETE_TODO,
  SELECT_TODO,
} from "../actions/constants";

export const STARTED = "STARTED";
export const UNSTARTED = "UNSTARTED";
export const COMPLETE = "COMPLETE";

let initial_state = {
  todos: [
    {
      title: "create react app",
      description: "Test react and electron",
      status: UNSTARTED,
    },
  ],
  editTodo: null,
};

function reducer(state = initial_state, action) {
  switch (action.type) {
    case SET_TODOS_LIST:
      return { ...state, todos: action.payload };
    case ADD_TODO:
      let copy_todos = state.todos.concat(action.payload);
      return { ...state, todos: copy_todos, editTodo: null };
    case DELETE_TODO:
      let copy_todos2 = state.todos.filter(
        (t) => t.title !== action.payload.title
      );
      return { ...state, todos: copy_todos2, editTodo: null };
    case UPDATE_TODO:
      let copy_todos3 = state.todos.map((t) => {
        if (t.title === state.editTodo.title) {
          return action.payload;
        }
        return t;
      });
      return { ...state, todos: copy_todos3, editTodo: null };
    case SELECT_TODO:
      return { ...state, editTodo: action.payload };
    default:
      return state;
  }
}

export default reducer;
