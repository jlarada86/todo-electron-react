import React, { useEffect } from "react";
import { Card, Button } from "react-bootstrap";

function Todo(props) {
  useEffect(() => {
    
  }, [props.todo]);

  const onEditClick = () => {
    props.edit(props.todo);
  };

  const onDelClick = () => {
    props.del(props.todo);
  };

  return (
    <div className="col-md-3 p-1">
      <Card>
        <Card.Body>
          <Card.Title>{props.todo.title}</Card.Title>
          <Card.Text>
            <span>{props.todo.description}</span>
            <br />
            <span>{props.todo.status}</span>
            <br />
          </Card.Text>
          <div >
            <Button variant="primary" onClick={onEditClick} className="m-1">
              Edit
            </Button>
            <Button variant="danger" onClick={onDelClick}>
              Delete
            </Button>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Todo;
