import React, { useEffect} from 'react'
import { connect } from "react-redux";
import Todo from "./todo";
import { updateTODO, deleteTODO, selectTODO } from '../actions';

function ListTodos(props) {

    const editTODO = (todo) => {
        
        let new_todo = { ...todo}
        //new_todo.description = 'change description';
        props.selectTodo(new_todo); 
    }

    const delTODO = (todo) => {
        
        props.deleteTodo(todo)
    }

    useEffect(() => {
        
    }, [props.todos])

    return (
        <div className="row">
            {props.todos.map(
                (t) => <Todo key={t.title} todo={t} edit={editTODO} del={delTODO}/>
                )}
        </div>
    )
}

const mapStateToProps = state => (
    {
        ...state
    }
)

const mapDispatchToProps = (dispatch) => ({
    updateTodo: (payload) => dispatch(updateTODO(payload)),
    deleteTodo: (payload) => dispatch(deleteTODO(payload)),
    selectTodo: (payload) => dispatch(selectTODO(payload)), 
});

export default connect(mapStateToProps, mapDispatchToProps)(ListTodos);