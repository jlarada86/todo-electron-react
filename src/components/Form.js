import React, { useEffect, useReducer, useState } from "react";
import { connect } from "react-redux";
import { Form, Button } from "react-bootstrap";
import { STARTED, UNSTARTED, COMPLETE } from "../reducers";
import { updateTODO, addTODO } from "../actions";

const initialState = {
  title: "",
  description: "",
  status: UNSTARTED,
};

const reducer = (state, { field, value }) => {
  return { ...state, [field]: value };
};

function TodoForm(props) {
  const [errors, setErrors] = useState({ title: "", description: "" });

  const [state, dispatch] = useReducer(reducer, initialState);

  const validForm = () => {
    let titleError = "";
    let descriptionError = "";
    if (title === "") {
      titleError = "Title is required";
    }
    if (description === "") {
      descriptionError = "Description is required";
    }
    setErrors({ title: titleError, description: descriptionError });
    if (titleError !== "" || descriptionError !== "") {
      return false;
    }

    return true;
  };

  const onSubmit = (evt) => {
    evt.preventDefault();

    const todo = {
      title: title,
      description: description,
      status: status,
    };

    if (validForm()) {
      if (props.selectTodo) {
        props.updateTodo(todo);
      } else {
        props.addTodo(todo);
      }
      dispatch({ field: "title", value: "" });
      dispatch({ field: "description", value: "" });
      dispatch({ field: "status", value: UNSTARTED });
    } else {
    }
  };

  const onChange = (e) => {
    dispatch({ field: e.target.name, value: e.target.value });
  };

  const { title, description, status } = state;

  useEffect(() => {
    ///console.log('props', props)
    if (props.selectTodo) {
      dispatch({ field: "title", value: props.selectTodo.title });
      dispatch({ field: "description", value: props.selectTodo.description });
      dispatch({ field: "status", value: props.selectTodo.status });
    } else {
      dispatch({ field: "title", value: "" });
      dispatch({ field: "description", value: "" });
      dispatch({ field: "status", value: UNSTARTED });
    }
  }, [errors, props.selectTodo]);

  return (
    <div>
      <h3>Form</h3>
      <Form onSubmit={onSubmit}>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter title"
            value={title}
            onChange={onChange}
            name="title"
          />
          <span className="text-danger">{errors.title}</span>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter description"
            value={description}
            onChange={onChange}
            name="description"
          />
          <span className="text-danger">{errors.description}</span>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label>Status</Form.Label>
          <Form.Control
            as="select"
            value={status}
            onChange={onChange}
            name="status"
          >
            <option value={UNSTARTED}>UNSTARTED</option>
            <option value={STARTED}>STARTED</option>
            <option value={COMPLETE}>COMPLETE</option>
          </Form.Control>
        </Form.Group>
        <Button variant="primary" onClick={onSubmit}>
          {props.selectTodo ? <span>EDIT</span> : <span>CREATE</span>}
        </Button>
      </Form>
    </div>
  );
}

const mapStateToProps = (state) => ({
  selectTodo: state.editTodo,
});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (payload) => dispatch(addTODO(payload)),
  updateTodo: (payload) => dispatch(updateTODO(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);
