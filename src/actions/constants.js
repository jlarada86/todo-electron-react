export const SET_TODOS_LIST = 'set_todos_list';
export const ADD_TODO = 'add_todo';
export const UPDATE_TODO = 'update_todo';
export const DELETE_TODO = 'delete_todo';
export const SELECT_TODO = 'select_todo';