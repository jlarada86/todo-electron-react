const { SET_TODOS_LIST, ADD_TODO, DELETE_TODO, UPDATE_TODO, SELECT_TODO } = require("./constants")

export const setListTODO = (payload) => {
    return { type: SET_TODOS_LIST, payload}
}

export const addTODO = (payload) => {
    return { type: ADD_TODO, payload}
}

export const updateTODO = (payload) => {
    return { type: UPDATE_TODO, payload }
}

export const deleteTODO = (payload) => {
    return { type: DELETE_TODO, payload}
}

export const selectTODO = (payload) => {
    return { type: SELECT_TODO, payload}
}


