import React, { useEffect, useState } from "react";
import logo from "./logo.svg";
import { connect } from "react-redux";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { Container } from "react-bootstrap";
import { addTODO } from "./actions";
import { STARTED, UNSTARTED, COMPLETE } from "./reducers";
import ListTodos from "./components/listTodos";
import TodoForm from "./components/Form";
import Menu from "./components/Menu";
import comunicate from "./utils/comunicate";


function App(props) {

  useEffect(() => {
    console.log("Dentro del useEffect");
    comunicate.importElectronIPC().send("enviar", props.todos);
    comunicate.importElectronIPC().on("load-data", (e, data) => {
      console.log("Load data", data);
    });
  }, []);

  return (
    <Container className="col-12 c1">
      <Menu />
      <h1>TODOS APP</h1>

      <TodoForm />
      <ListTodos />
    </Container>
  );
}

const mapStateToProps = (state) => ({
  ...state,
});

export default connect(mapStateToProps)(App);
